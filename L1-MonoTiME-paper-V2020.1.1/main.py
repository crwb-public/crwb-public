import framework
from time import time
from framework import *
from framework.std import ByIdIfNotSeen, ByType, CalcDelta, ArithMean, PriceObservation
import csv
S1 = framework.Set()
with open('data.csv', newline='') as csvfile:
    reader = iter(csv.reader(csvfile, delimiter=',', quotechar='|'))
    for row in reader:
        S1.append(PriceObservation(row[1], float(row[2]), int(row[0].split("_")[1])))
calcdelta = CalcDelta()
arithmeticmean = ArithMean()
byprodid = ByIdIfNotSeen()
alldelta = ByType("delta")
t0 = time()
framework.main_globals = globals()
framework.MonoTIME_RUN("union( calcdelta(Clumpby( byprodid )(S1)) , S1)")
framework.MonoTIME_RUN("union( arithmeticmean(Clumpby( alldelta )(S1)) , S1)")
print(S1[-1].price)
print(time() - t0)