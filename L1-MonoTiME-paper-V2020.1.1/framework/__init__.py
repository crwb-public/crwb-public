from abc import ABC, abstractmethod
from copy import copy
import ast

main_globals = None

class Side_effect_Object(object):
    def __init__(self):
        pass

class TData():
    pass

class Set(list):
    pass

class Feed(list):
    pass


class Criteria(ABC):
    def __init__(self):
        self.soft_buffer = {}
        self.hard_buffer = {}

    @abstractmethod
    def __call__(self, data_set : [TData], data : TData) -> bool:
        return False

    def reset(self):
        self.soft_buffer = {}

class Transformation(ABC):
    @abstractmethod
    def __call__(self, feed : Feed) -> [TData]:
        return []

class Clumpby():
    def __init__(self, criteria):
        self.criteria = criteria

    def apply_clumpby(self,set_data: [TData]) -> Feed:
        new_feed = Feed()

        set_data_it = iter(set_data)
        if "iterator" in self.criteria.hard_buffer:
            set_data_it = copy(self.criteria.hard_buffer["iterator"])

        try:
            while True:
                d = next(set_data_it)
                if self.criteria(set_data,d):
                    new_feed.append(d)
        except StopIteration:
            pass
        #for d in set_data_it:
        #    if self.criteria(set_data,d):
        #        new_feed.append(d)
        self.criteria.reset()
        return new_feed

    def __call__(self, data_set : [TData]) -> Feed:
        return self.apply_clumpby(data_set)


def applyC( clumpby: Clumpby, data_set: [TData]) -> Feed:
    answer = clumpby(data_set)
    return answer

def applyT( transformation: Transformation, feed: Feed) -> [TData]:
    answer = transformation(feed)
    return answer

def union(TDataSet:Set, TDataSet2:Set):
    for td in TDataSet:
        if td not in TDataSet2:
            TDataSet2.append(td)
    return TDataSet2

def continueUntilStopCondition( data_set : [TData] , clumpby : Clumpby, transformation: Transformation) -> None:
    found_stop_point = False
    while not found_stop_point:
        feed = apply_c(clumpby, data_set)
        if feed == []:
            found_stop_point = True
        else:
            data_set_length = len(data_set)
            data_set += applyT(transformation, feed)
            if(data_set_length == len(data_set)):
                found_stop_point = True


class DepthSetter(ast.NodeVisitor):
    def visit(self,node):
        depth = 0
        c = [node]
        n = []
        while len(c) > 0:
            cnode = c.pop(0)
            cnode.depth = depth
            for child in ast.iter_child_nodes(cnode):
                n.append(child)
            if len(c) == 0:
                depth += 1
                c = n
                n = []

class GetInfo(ast.NodeVisitor):
    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.found = []

    def visit_Name(self, node):
        self.found.append((node.id, node.depth))
        self.generic_visit(node)

file_counter = 0

def MonoTIME_RUN( execution_tree ) -> None:
    global main_globals
    global file_counter
    assert(main_globals is not None)
    for line in execution_tree.split("\n"):
        tree = ast.parse(line)
        dset = DepthSetter()
        dset.visit(tree)
        gnames = GetInfo()
        gnames.visit(tree)
        g = main_globals
        candidate_feed = None
        candidate_depth = -1
        for f,d in gnames.found:
            if f not in g:
                pass
            else:
                if issubclass(type(g[f]), Set):
                    if candidate_feed is None or candidate_depth < d:
                        candidate_feed = f
                        candidate_depth = d
        if candidate_feed is not None:
            old_len = -1
            tree = ast.parse(candidate_feed + " = " + line)
            compiled = compile(tree, filename="<ast_{}>".format(file_counter), mode="exec")
            while old_len != len(g[candidate_feed]):
                old_len = len(g[candidate_feed])
                exec(compiled,g)