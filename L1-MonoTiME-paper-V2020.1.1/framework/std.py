from framework.__init__ import TData, Feed, Transformation, Criteria

class PriceObservation(TData):
    def __init__(self, time, price, _id):
        self.time = time
        self.price = price
        self.id = _id
    def __str__(self):
        return "<{},{},{}>".format(self.id, self.time, self.price)

class Delta(TData):
    def __init__(self, time_interval, price, _id):
        assert(len(time_interval) == 2)
        self.time = time_interval
        self.price = price
        self.id = _id
        self.type = "delta"
    def __str__(self):
        return "<delta, {},{},{}>".format(self.id, self.time, self.price)
    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.time == other.time and self.id == other.id
        else:
            return False
    def __ne__(self, other):
        return not self.__eq__(other)

class Average(TData):
    def __init__(self, time_interval, price, multiple_ids):
        assert(len(time_interval) >= 2)
        assert (len(time_interval) <= len(multiple_ids))
        self.time = time_interval
        self.price = price
        self.ids = multiple_ids
        self.type = "average"
    def __str__(self):
        return "<average, {},{},{}>".format(self.ids, self.time, self.price)
    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.time == other.time and self.ids == other.ids
        else:
            return False
    def __ne__(self, other):
        return not self.__eq__(other)
###

class CalcDelta(Transformation):
    def __call__(self, data: Feed) -> [TData]:
        if (len(data) == 2):
            time_interval = [min(data[0].time, data[1].time), max(data[0].time, data[1].time)]
            value = data[1].price - data[0].price
            _id = data[1].id
            new_data = Delta(time_interval,value,_id)
            return [new_data]
        else:
            return []

class ArithMean(Transformation):
    def __call__(self, data: Feed) -> [TData]:
        if (len(data) != 0):
            value = 0
            time_interval = data[0].time
            ids = []
            for current_data in data:
                value += current_data.price
                ids.append(current_data.id)
            if len(data) > 0:
                value = value/len(data)
            new_data = Average(time_interval, value, ids)
            return [new_data]
        else:
            return []

class ByIdIfNotSeen(Criteria):
    def __call__(self, set_data: [TData], data: TData) -> bool:
        if not "choosen_id" in self.soft_buffer:
            if (not "iterator" in self.hard_buffer) or \
            ("iterated_set" in self.hard_buffer and self.hard_buffer["iterated_set"] is not set_data):
                self.hard_buffer["iterator"] = iter(set_data)
                self.hard_buffer["iterated_set"] = set_data
                self.hard_buffer["seen"] = {}
            for c_data in self.hard_buffer["iterator"]:
                if (not hasattr(c_data, "type")):
                    if c_data.id not in self.hard_buffer["seen"]:
                        self.hard_buffer["seen"][c_data.id] = True
                        self.soft_buffer["choosen_id"] = c_data.id
                        break
        if not "choosen_id" in self.soft_buffer:
            answer = False
        else:
            answer = data.id == self.soft_buffer["choosen_id"]
        return answer

class ByType(Criteria):
    def __init__(self, type):
        super(self.__class__, self).__init__()
        self.type = type

    def __call__(self, set_data: [TData], data: TData) -> bool:
        if data in set_data and hasattr(data, "type") and data.type == self.type:
            return True
        else:
            return False