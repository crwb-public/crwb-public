import csv
from time import time
with open('data.csv', newline='') as csvfile:
    S1 = []
    reader = iter(csv.reader(csvfile, delimiter=',', quotechar='|'))
    for row in reader:
        S1.append([int(row[1]), float(row[2]), int(row[0].split("_")[1])])
t0 = time()
new_answers = []
seen = []
for entry in S1:
    if entry[0] != "delta" and entry[2] not in seen:
        found = False
        seen.append(entry[2])
        for entry2 in S1:
            if entry2[0] != "delta" and entry[2] == entry2[2] and entry[0] != entry2[0]:
                if entry[0] < entry2[0]:
                    difference = entry2[1] - entry[1]
                else:
                    difference = entry[1] - entry2[1]
                S1.append(["delta", [entry[0],entry2[0]], difference, entry[2]])
seen = []
for entry in S1:
    if entry[0] == "delta":
        seen.append(entry[2])
answer = sum(seen)/len(seen)
print(answer)
print(time() - t0)